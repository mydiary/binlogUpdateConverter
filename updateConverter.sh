#!/bin/bash

	help()
	{
		cat <<- HELP
			  This is a Mysql binlog update sql parse tool.
			  USAGE EXAMPLE: ./binlogUpdateConventer.sh [-p] oraginsql.sql | -h
			  OPTIONS: -h help text
				   -p the update sql from Mysql binlog
		HELP
	   	exit 0
	}
	
	while [ -n "$1" ]; do
	case "$1" in
	   -h) help;shift 1;; # function help is called
	   -p) binlog=$2;shift 2;; # variable opt_f is set
	   --) shift;break;; # end of options
	   -*) echo "error: no such option $1. -h for help";exit 1;;
	   *)  binlog=$1; break;;
	esac
	done
	
#判断某元素是否在数组中
	function hasItemInArray()
	{
		local item
		local array=`echo "$1"`
	
		for tmp in ${array[*]}; do
			if test "$2" = "$tmp"; then
				return 1
			fi
		done
		return 0
	}



	function configParse()
	{
		local pos=c

		while read confLine		
		do
			if [[ -z "$confLine" || "#" == "${confLine:0:1}" ]]; then
				continue
			elif [[ "$confLine" == "[columns]" ]]; then
				pos=c
			elif [[ "$confLine" == "[where_columns]" ]]; then	
				pos=w
			elif [[ "$confLine" == "[set_columns]" ]]; then	
				pos=s
			else
				local i
				case $pos in
					c)
						i=${#columns[*]}
						columns[i]=$confLine
					;;
					w)
						i=${#where_col[*]}
						where_col[i]=$confLine
					;;
					s)
						i=${#set_col[*]}
						set_col[i]=$confLine
					;;
				esac
			fi

		done < $1
	}



	file="new.sql"
	rm -rf new.sql

	columns=()
	set_col=()
	where_col=()

	configParse ./config


	size=${#columns[*]}
	set_size=${#set_col[*]}
	where_size=${#where_col[*]}



#当前遍历sql语句的位置
	head_flag=1
	pos="set"
#遍历变量的位置
	count=1
#sql语句的后缀
	suffix=","

	sed -i 's/\/\*.*\*\///' $binlog
	cat $binlog | while read LINE
	do
		if [[ $LINE =~ "WHERE" ]]; then
			echo "SET" >> new.sql
			pos="set"	
			suffix=","
			count=1

		elif [[ $LINE =~ "SET" ]]; then
			echo " WHERE (" >> new.sql
			pos="where"
			suffix="and"
			count=1

		elif [[ $LINE =~ "@" ]]; then
			tmp=${LINE#*@}
			num=`expr $tmp : '\([0-9]*\)'`
			value=${tmp#*=}
#			echo $num - ${columns[$num -1]} - $value

			column=${columns[$num - 1]}
			
			if [ $pos == "where" ]; then
				array=`echo ${where_col[*]}` 
			else 
				array=`echo ${set_col[*]}` 
			fi
#			echo "array:"$array==========$column
			hasItemInArray "$array" "$column"
			hasItem=$?
			if test "$hasItem" = 1; then
				echo ${columns[$num - 1]}"="$value >> $file
				if [ $pos == "where" ]; then
					size=$where_size		
				else 
					size=$set_size
				fi
				if [ "${count-1}" != "$size" ]; then
					echo $suffix >> $file
					let count=$count+1
				else
					if [ $pos == "where" ]; then
						echo ");" >> $file
					fi
	
				fi
			fi


		else 
			if [ $head_flag == 1 ]; then
				head_flag=0
				echo $LINE
			fi
			echo  >> $file 
			echo ${LINE:4} >> new.sql
		fi
	done


