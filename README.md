#binlogUpdateConverter

此脚本用来将从binlog中导出的update sql语句翻转为与之相反的update语句，来恢复更新出错的记录。

例：
从binlog导出的update语句如下:
### UPDATE `test`.`binlog_test`
### WHERE
###   @1=1 
###   @2=1000 
###   @3='A' 
###   @4='test1' 
### SET
###   @1=1 
###   @2=1000 
###   @3='Z' 
###   @4='test1' 
### UPDATE `test`.`binlog_test`
### WHERE
###   @1=2 
###   @2=1001 
###   @3='B' 
###   @4='test2' 
### SET
###   @1=2 
###   @2=1001 
###   @3='Z' 
###   @4='test2' 


步骤1> 在config文件中配置上面update语句对应的表的全部列和需要在新update语句中添加的列，如下：
[columns]
id
int_col
char_col
varchar_col

#       此标签下配置生成的update语句中where条件包含的列
[where_columns]
id


#       此标签下配置生成的update语句中set包含的列
[set_columns]
int_col
char_col


步骤2> 执行转换脚本
$updateConverter.sh origain.sql

步骤3> 在文件夹中会生成新的文件new.sql，里面就是存放的新的sql脚本，内容如下:

UPDATE `test`.`binlog_test`
SET
int_col=1000
,
char_col='A'
 WHERE (
id=1
);

UPDATE `test`.`binlog_test`
SET
int_col=1001
,
char_col='B'
 WHERE (
id=2
);
